export const NewsDetail = (result) => {
  return {
    type: "NEWS_DETAIL",
    result,
  };
};
